import 'package:flutter/widgets.dart';

class Settings {
  // Dark theme colors.
  static const Color COLOR_DARK_PRIMARY = Color(0XFF0C252E);
  static const Color COLOR_DARK_SECONDARY = Color(0XFF09191F);
  static const Color COLOR_DARK_HIGHLIGHT = Color(0XFF257088);
  static const Color COLOR_DARK_SHADOW = Color(0XFF040F13);
}
